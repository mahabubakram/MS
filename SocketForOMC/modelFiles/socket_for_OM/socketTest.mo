model socketTest "Model to create a TCP socket to specified IPAddress and port"
  function sendMessage "send a message through the socket"
    input String message;
  
    external "C" sendMessageViaSocket(message);
    annotation(Include = "#include <Socket.h>", Library = {"SocketModelica", "wsock32"});
  end sendMessage;

  function receiveMessage "receive a message through the socket"
    output String message;
  
    external "C" message = receiveMessage();
    annotation(Include = "#include <Socket.h>", Library = {"SocketModelica", "wsock32"});
  end receiveMessage;

  function createSocket "Create the TCP socket"
    input String IPAddress;
    input Real port;
  
    external "C" createSocket(IPAddress, port);
    annotation(Include = "#include <Socket.h>", Library = {"SocketModelica", "wsock32"});
  end createSocket;

  function startUP "Initialize the Windows Socket Library"

    external "C" startUP();
    annotation(Include = "#include <Socket.h>", Library = {"SocketModelica", "wsock32"});
  end startUP;

  parameter String IPAddress = "127.0.0.1" "IP-Address of Server";
  parameter Integer port = 8888 "Port to connect to";
  Real x;
equation
  x = time;
  startUP();
  createSocket(IPAddress, port);
  annotation(Diagram, Icon(Rectangle(extent = [-40, -20; 2, -80], style(color = 0, rgbcolor = {0, 0, 0}, thickness = 4, fillColor = 10, rgbfillColor = {135, 135, 135}, fillPattern = 1)), Rectangle(extent = [-40, 20; 40, -20], style(color = 0, rgbcolor = {0, 0, 0}, thickness = 4, fillColor = 10, rgbfillColor = {135, 135, 135}, fillPattern = 1)), Ellipse(extent = [36, 20; 46, -20], style(color = 0, rgbcolor = {0, 0, 0}, thickness = 4, fillColor = 10, rgbfillColor = {135, 135, 135}, fillPattern = 1)), Text(extent = [-60, 80; 60, 40], style(color = 0, rgbcolor = {0, 0, 0}, thickness = 4, fillColor = 10, rgbfillColor = {135, 135, 135}, fillPattern = 1), string = "%=IPAddress"), Text(extent = [-62, 52; 10, 30], style(color = 0, rgbcolor = {0, 0, 0}, thickness = 4, fillColor = 10, rgbfillColor = {135, 135, 135}, fillPattern = 1), string = "%=port")));
end socketTest;