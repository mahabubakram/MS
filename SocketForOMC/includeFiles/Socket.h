int createSocket(const char* IP_ADDRESS, int port);
int startUP();
void sendMessageViaSocket(char* buf);
char* receiveMessage();
int cleanUp();
