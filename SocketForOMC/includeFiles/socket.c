#include<io.h>
#include <stdio.h>
#include <conio.h>
#include "winsock2.h"

#pragma comment(lib,"ws2_32.lib") //Winsock Library
 
static SOCKET s=NULL, new_socket=NULL;
static char receiveBuffer[255];


//activate socket funcionality
int startUP()
{
	WSADATA wsa;
	return WSAStartup(MAKEWORD(2,0),&wsa);
}

//send a string through socket
void sendMessageViaSocket(char* buf)
{
	
	send(new_socket, buf, strlen(buf),0);
}


//receive a string through socket
char* receiveMessage()
{
	recv(s,receiveBuffer,255,0);

	return receiveBuffer;
}


//create a socket to IP-Address and port
int createSocket(const char* IP_ADDRESS, int port)
{
	SOCKADDR_IN addr, client;	

	s=socket(AF_INET,SOCK_STREAM,0);

	memset(&addr,0,sizeof(SOCKADDR_IN));

	
	addr.sin_family=AF_INET;
	addr.sin_port=htons(port); 
	addr.sin_addr.s_addr=inet_addr(IP_ADDRESS); 


	if( bind(s ,(struct sockaddr *)&addr , sizeof(addr)) == SOCKET_ERROR)
	{
		printf("Bind failed with error code : %d" , WSAGetLastError());
	}
	else{
		puts("Bind done");
		//waiting for the connection
		listen(s , 5);

		//create new socket to send Message
		int sizeStruct = sizeof(struct sockaddr_in);
		new_socket = accept(s , (struct sockaddr *)&client, &sizeStruct);
		if (new_socket == INVALID_SOCKET)
		{
			printf("accept failed with error code : %d" , WSAGetLastError());
		}
		else{
			char *buff = "hello from Server";
			sendMessageViaSocket(buff);
			puts("New socket created");
			return 1;
		}

		return 0;

	}
	return 0;

}